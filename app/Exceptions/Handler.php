<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    
    // public function render($request, Exception $exception)
    // {
    //     // if ($exception instanceof \Illuminate\Session\TokenMismatchException) {
    //     //     return redirect('/login');
    //     // }
    //     if (method_exists($exception, 'getStatusCode') && $exception->getStatusCode() == '419') {
    //         return redirect()->route('login');
    //     }

    //     return parent::render($request, $exception);
    // }

    // protected function unauthenticated($request, AuthenticationException $exception)
    // {
    //     if ($request->expectsJson()) {
    //         return response()->json(['error' => 'Unauthenticated.'], 401);
    //     }
    //     return redirect()->guest('login');
    // }
}

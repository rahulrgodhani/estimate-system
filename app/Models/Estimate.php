<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Estimate extends Model
{
    use HasFactory;
    protected $table = 'estimates';
    public $timestamps = false;
    protected $fillable = [
        'retailer_id', 'voucher_no', 'products', 'qty', 'rate', 'discount', 'price', 'total_amt', 'round', 'other_charge', 'other_charge_value', 'batch', 'batch_stock', 'delivery_date', 'gen_date', 'gen_by', 'delete_status'
    ];

    protected $attributes = [
        'delete_status' => 0,
    ];
}

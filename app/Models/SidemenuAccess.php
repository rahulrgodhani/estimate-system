<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SidemenuAccess extends Model
{
    use HasFactory;
    protected $table = 'sidemenu_access';
    public $timestamps = false;
    protected $fillable = [
        'page_id', 'ws_status', 'user_id', 'admin_status'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'settings';
    protected $fillable = [
        'main_logo', 'icon', 'favicon_icon', 'name', 'email', 'number', 'address', 'city', 'district', 'state', 'state_code', 'pincode', 'gst_no', 'pan_no', 'registration_date', 'logo_view', 'charges_type', 'user_limit', 'product_limit', 'cproduct_limit'
    ];
}

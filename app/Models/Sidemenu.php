<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sidemenu extends Model
{
    use HasFactory;
    protected $table = 'sidemenu';
    public $timestamps = false;
    protected $fillable = [
        'page_name', 'parent_id', 'route_name', 'route_reference', 'icon', 'status', 'ordering'
    ];
}

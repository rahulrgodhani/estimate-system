<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\SidemenuAccess;
use Illuminate\Support\Facades\Route;

class userAuthOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $userSidebarAccess = SidemenuAccess::join('sidemenu', 'sidemenu_access.page_id', '=', 'sidemenu.id')->where('sidemenu_access.ws_status', 'worker')->where('sidemenu.status', 1)->where('sidemenu_access.admin_status', 1)->where('sidemenu_access.user_id', auth()->user()->id)->orderBy('sidemenu.ordering', 'ASC')->pluck('sidemenu.route_reference')->toArray();

        array_push($userSidebarAccess, 'stock', 'noauth', 'password', 'profile', 'logout', 'ajax');

        $routename = Route::currentRouteName();
        $routenamearr = explode('.', $routename);
        $routename = $routenamearr[0];

        if (in_array($routename, $userSidebarAccess)) {    //  || !($request->isMethod('get'))
            return $next($request);
        } else {
            return redirect()->route('noauth');
        }
    }
}

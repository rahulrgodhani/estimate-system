<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\View\View;
use GuzzleHttp\Client;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Http;

class PasswordResetLinkController extends Controller
{

    protected $tomail;
    /**
     * Display the password reset link request view.
     */
    public function create(): View
    {
        return view('auth.forgot-password');
    }

    /**
     * Handle an incoming password reset link request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'email' => ['required', 'email'],
        ]);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $status = Password::sendResetLink(
            $request->only('email')
        );

        return $status == Password::RESET_LINK_SENT
            ? back()->with('status', __($status))
            : back()->withInput($request->only('email'))
            ->withErrors(['email' => __($status)]);
    }

    public function sendotp(Request $request)
    {
        $input = $request->email;
        $request->session()->put('input', $input);
        $user = User::where('email', $input)->orWhere('phone', $input);
        if ($user->count() == 1) {
            $otp = rand(111111, 999999);
            $enterduser = $user->first();
            $request->session()->put('user_id', $enterduser->id);
            $enterduser->otp = $otp;
            $enterduser->otp_limit = date('Y-m-d H:i:s', time() + 300);
            $enterduser->save();
            $validTime = date("h:i:sa", time() + 300);

            if (strlen($input) == 10 && is_numeric($input)) {

                $authKey = env('SMS_AUTH_KEY');
                $senderId = env('SMS_SENDER_ID');
                $dlt_te_id = env('SMS_DLT_TE_ID');
                $route = "4";
                $mobileNumber = "91" . $input;

                $msg = "Your OTP to reset stockify password is " . $otp . " The OTP is valid until " . $validTime . " Thank you";
                $message = urlencode($msg);
                $postData = array(
                    'mobiles' => $mobileNumber,
                    'sender' => $senderId,
                    'message' => $message,
                    'authkey' => $authKey,
                    'route' => $route,
                    'DLT_TE_ID' => $dlt_te_id,
                );
                $url = "https://api.msg91.com/api/sendhttp.php";

                $ch = curl_init();
                curl_setopt_array($ch, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_POST => true,
                    CURLOPT_POSTFIELDS => $postData
                ));
                //Ignore SSL certificate verification
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                //get response
                $output = curl_exec($ch);

                if (curl_errno($ch)) {
                    return back()->with('error', 'Error sending otp, try another method.');
                }
                curl_close($ch);

                return redirect()->route('password.step2')->with('status', 'please check your phone for otp.');
            } else {
                $this->tomail = $enterduser->email;
                Mail::send('mail.reset-password', array('validity' => $validTime, 'otp' => $otp), function ($message) {
                    $message->to($this->tomail, 'User')->subject('Reset Password Otp!');
                    $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
                });

                return redirect()->route('password.step2')->with('status', 'please check your email.');
            }
        } else {
            return back()->with('error', 'Please enter valid email or phone number');
        }
    }

    public function resendotp(Request $request)
    {
        $input = $request->session()->get('input');
        $user = User::where('email', $input)->orWhere('phone', $input);
        if ($user->count() == 1) {
            $otp = rand(111111, 999999);
            $enterduser = $user->first();
            $request->session()->put('user_id', $enterduser->id);
            $enterduser->otp = $otp;
            $enterduser->otp_limit = date('Y-m-d H:i:s', time() + 300);
            $enterduser->save();
            $validTime = date("h:i:sa", time() + 300);

            if (strlen($input) == 10 && is_numeric($input)) {

                $authKey = "356339A6ijq5FJ17608164dfP1";
                $senderId = "INFSTK";
                $dlt_te_id = "1307161849072868685";
                $mobileNumber = "91" . $input;

                $msg = "Your OTP to reset stockify password is " . $otp . " The OTP is valid until " . $validTime . " Thank you";
                $message = urlencode($msg);
                $route = "4";
                $postData = array(
                    'mobiles' => $mobileNumber,
                    'sender' => $senderId,
                    'message' => $message,
                    'authkey' => $authKey,
                    'route' => $route,
                    'DLT_TE_ID' => $dlt_te_id
                );
                $url = "https://api.msg91.com/api/sendhttp.php";

                $ch = curl_init();
                curl_setopt_array($ch, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_POST => true,
                    CURLOPT_POSTFIELDS => $postData
                ));
                //Ignore SSL certificate verification
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                //get response
                $output = curl_exec($ch);

                if (curl_errno($ch)) {
                    return back()->with('error', 'Error sending otp, try another method.');
                }
                curl_close($ch);

                return redirect()->route('password.step2')->with('status', 'please check your phone for otp.');
            } else {
                $this->tomail = $enterduser->email;
                Mail::send('mail.reset-password', array('validity' => $validTime, 'otp' => $otp), function ($message) {
                    $message->to($this->tomail, 'User')->subject('Reset Password Otp!');
                    $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
                });

                return redirect()->route('password.step2')->with('status', 'please check your email.');
            }
        } else {
            return back()->with('error', 'Please enter valid email or phone number');
        }
    }

    public function step2(Request $request)
    {
        if ($request->isMethod('post')) {
            $user_id = $request->session()->get('user_id');
            if ($user_id) {
                $user = User::find($user_id);
                if ($user->otp_limit > date("Y-m-d H:i:s")) {
                    $otp = $request->otp1 . $request->otp2 . $request->otp3 . $request->otp4 . $request->otp5 . $request->otp6;
                    if ($user->otp == $otp) {
                        $token = Str::random(60);
                        $request->session()->put('token', $token);
                        $request->session()->forget('input');
                        return redirect()->route('password.reset', $token);
                    } else {
                        return back()->with('error', 'Invalid otp');
                    }
                } else {
                    $request->session()->forget('user_id');
                    return redirect()->route('password.request')->with('error', 'Otp Expired. Try again later.');
                }
            } else {
                return redirect()->route('password.request');
            }
        }

        if ($request->isMethod('get')) {
            return view('auth.step2');
        }
    }
}

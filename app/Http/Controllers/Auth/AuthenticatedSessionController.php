<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     */
    public function create(): View
    {
        return view('auth.login');
    }

    /**
     * Handle an incoming authentication request.
     */
    public function store(LoginRequest $request): RedirectResponse
    {
        if (User::where('phone', $request->phone)->count() > 0 && User::where('phone', $request->phone)->first()->login_attempts >= 3) {
            return redirect()->back()->with('limit', 'You reached your attempt limit! Kindly Reset password.');
        } else {
            $request->authenticate();

            $request->session()->regenerate();

            $user = User::where('phone', $request->phone)->first();
            $user->last_login = date("Y-m-d H:i:s");
            $user->login_attempts = 0;
            $user->save();

            return redirect()->intended(Auth::user()->type == 'worker' ? RouteServiceProvider::HOME : RouteServiceProvider::ADMIN_HOME);
        }
    }

    /**
     * Destroy an authenticated session.
     */
    public function destroy(Request $request): RedirectResponse
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}

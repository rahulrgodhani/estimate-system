<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileUpdateRequest;
use App\Models\Setting;
use App\Models\StateCode;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use Intervention\Image\Facades\Image;

class ProfileController extends Controller
{

    public function editUser(Request $request): View
    {
        return view('profile.edit', [
            'user' => $request->user(),
            'company' => Setting::find(1),
        ]);
    }

    public function updateUser(ProfileUpdateRequest $request): RedirectResponse
    {
        $request->user()->fill($request->validated());

        if ($request->has('image')) {
            empty($request->user()->image) ? '' : unlink(public_path('uploads/users/' . $request->user()->image));
            $image_name = uniqid() . '.' . $request->image->getClientOriginalExtension();
            $image_path = 'uploads/users/' . $image_name;
            Image::make($request->image)->resize(320, 320)->save(public_path($image_path));
            $request->user()->image = $image_name;
        }

        $request->user()->save();

        return Redirect::route(Auth::user()->type == 'owner' ? 'admin.profile.edit' : 'profile.edit')->with('status', 'Profile updated successfully');
    }

    public function updateCompany(Request $request)
    {
        $request->validate([
            'name' => ['required'],
            'phone' => ['required', 'numeric', 'digits:10'],
            'email' => ['required'],
            'address' => ['required'],
            'city' => ['required'],
            'district' => ['required'],
            'state' => ['required'],
            'pincode' => ['nullable', 'numeric', 'digits:6'],
        ]);

        $setting = Setting::find(1);
        $setting->name = $request->name;
        $setting->email = $request->email;
        $setting->number = $request->phone;
        $setting->address = $request->address;
        $setting->city = $request->city;
        $setting->district = $request->district;
        $setting->state = $request->state;
        $setting->state_code = $request->state_code;
        $setting->pincode = $request->pincode;
        $setting->gst_no = $request->gst_no;
        $setting->pan_no = $request->pan_no;
        $setting->save();

        return redirect()->back()->with('status', 'Company details updated successfully');
    }

    public function InvoiceSettings(Request $request)
    {
        $request->validate([
            'discount_type' => ['required'],
        ]);

        $setting = Setting::find(1);
        $setting->charges_type = $request->discount_type;
        $setting->save();

        return redirect()->back()->with('status', 'Invoice settings updated successfully');
    }

    public function getStateCode(Request $request)
    {
        $stateCode = StateCode::where('state_name', $request->steteName)->first();

        if ($stateCode) {
            return $stateCode->state_code;
        } else {
            return false;
        }
    }
}

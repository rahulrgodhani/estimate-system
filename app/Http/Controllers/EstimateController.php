<?php

namespace App\Http\Controllers;

use App\Models\Block;
use App\Models\Category;
use App\Models\Dispatch;
use App\Models\Estimate;
use App\Models\PackingType;
use App\Models\PriceListAmount;
use App\Models\Product;
use App\Models\Retailer;
use App\Models\RetailerDiscount;
use App\Models\Track;
use App\Models\Unit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use PDF;

class EstimateController extends Controller
{

    public function printEstimateSlip()
    {
        $data = Estimate::join('retailers', 'estimates.retailer_id', '=', 'retailers.id')->join('users', 'estimates.gen_by', '=', 'users.id')->orderBy('estimates.id', 'DESC')->get(['estimates.*', 'retailers.name as retailer_name', 'retailers.phone', 'users.name as user_name'])->toArray();
        // dd($data);
        return view('estimate.print-estimate-slip', ['data' => $data]);
    }

}

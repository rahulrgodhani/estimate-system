<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\SidemenuAccess;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    // user
    public function index()
    {
        return view('dashboard');
    }

    public function noauthUser()
    {
        return view('auth.not-authorized');
    }
}

<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\ProfileController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DispatchController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\EstimateController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RetailerController;
use App\Http\Controllers\StockController;
use App\Http\Controllers\SubCategoryController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\MaterialController;
use App\Http\Controllers\PackingTypeController;
use App\Http\Controllers\RecepieController;
use App\Http\Controllers\UnitController;

Route::middleware('guest')->group(function () {
    // Login
    Route::get('/', [AuthenticatedSessionController::class, 'create'])->name('login');
    Route::post('login', [AuthenticatedSessionController::class, 'store'])->name('login.post');
    // Reset Password
    Route::get('forgot-password', [PasswordResetLinkController::class, 'create'])->name('password.request');
    Route::post('forgot-password', [PasswordResetLinkController::class, 'sendotp'])->name('password.email');
    Route::get('forgot-password/resend', [PasswordResetLinkController::class, 'resendotp'])->name('password.resend');
    Route::get('forgot-password/step2', [PasswordResetLinkController::class, 'step2'])->name('password.step2');
    Route::post('forgot-password/step2', [PasswordResetLinkController::class, 'step2'])->name('password.step2');
    Route::get('reset-password/{token}', [NewPasswordController::class, 'create'])->name('password.reset');
    Route::post('reset-password', [NewPasswordController::class, 'storePassword'])->name('password.store');
});

// User Routes
Route::middleware('auth', 'users')->group(function () {

    Route::get('/profile', [ProfileController::class, 'editUser'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'updateUser'])->name('profile.update');
    Route::post('/profile/get-statecode', [ProfileController::class, 'getStateCode'])->name('profile.get-statecode');
    Route::put('password', [PasswordController::class, 'update'])->name('password.update');
    Route::post('logout', [AuthenticatedSessionController::class, 'destroy'])->name('logout');

    Route::get('/dashboard', [HomeController::class, 'index'])->name('dashboard');

    Route::get('/print-estimate-slip', [EstimateController::class, 'printEstimateSlip'])->name('print-estimate-slip');
});

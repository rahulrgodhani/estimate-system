<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\ConfirmablePasswordController;


Route::get('/', function () {
    return redirect('login');
});

// confirm password required steps add password.confirm middleware
Route::get('confirm-password', [ConfirmablePasswordController::class, 'show'])->name('password.confirm');
Route::post('confirm-password', [ConfirmablePasswordController::class, 'store'])->name('password.confirm');

// no authority pages
Route::get('/not-authorized', [HomeController::class, 'noauthUser'])->name('noauth');

// user routes 
require __DIR__ . '/auth.php';

// admin routes 
require __DIR__ . '/adminauth.php';

<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\PasswordController;
use App\Http\Controllers\Auth\ProfileController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DispatchController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\EstimateController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\MaterialController;
use App\Http\Controllers\PackingTypeController;
use App\Http\Controllers\PriceListController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\PurchaseController;
use App\Http\Controllers\RecepieController;
use App\Http\Controllers\RetailerController;
use App\Http\Controllers\StockController;
use App\Http\Controllers\SubCategoryController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\UnitController;
use Illuminate\Support\Facades\Route;

// Admin routes
Route::group(['middleware' => ['auth', 'admins'], 'prefix' => '_admin', 'as' => 'admin.'], function () {

    // profile & update password & logout
    Route::get('/profile', [ProfileController::class, 'editUser'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'updateUser'])->name('profile.update');
    Route::post('/profile/get-statecode', [ProfileController::class, 'getStateCode'])->name('profile.get-statecode');
    Route::patch('/profile-company', [ProfileController::class, 'updateCompany'])->name('profile.update-company');
    Route::patch('/invoice-settings', [ProfileController::class, 'InvoiceSettings'])->name('profile.invoice-settings');
    Route::put('password', [PasswordController::class, 'update'])->name('password.update');
    Route::post('logout', [AuthenticatedSessionController::class, 'destroy'])->name('logout');

    Route::get('/dashboard', [HomeController::class, 'index'])->name('dashboard');
    Route::get('/print-estimate-slip', [EstimateController::class, 'printEstimateSlip'])->name('print-estimate-slip');
});

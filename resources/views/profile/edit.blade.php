@extends('layouts.main')

@section('title', 'Profile')

@section('head')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/page-profile.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/page-profile.min.css') }}">
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.13/cropper.min.js" integrity="sha512-6lplKUSl86rUVprDIjiW8DuOniNX8UDoRATqZSds/7t6zCQZfaCe3e5zcGaQwxa8Kpn5RTM9Fvl3X2lLV4grPQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.3/dropzone.min.js" integrity="sha512-U2WE1ktpMTuRBPoCFDzomoIorbOyUv0sP8B+INA3EzNAhehbzED1rOJg6bCqPf/Tuposxb5ja/MAUnC8THSbLQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.13/cropper.min.css" integrity="sha512-cyzxRvewl+FOKTtpBzYjW6x6IAYUCZy3sGP40hn+DQkqeluGRCax7qztK2ImL64SA+C7kVWdLI6wvdlStawhyw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.3/dropzone.min.css" integrity="sha512-jU/7UFiaW5UBGODEopEqnbIAHOI8fO6T99m7Tsmqs2gkdujByJfkCbbfPSN4Wlqlb9TGnsuC0YgUgWkRBK7B9A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
    .userImage {
        border: 1px solid #1f2358 !important;
    }

    .imageclass {
        width: 200px;
        float: right;
    }

    #cropping_image {
        display: block;
        max-width: 100% !important;
        height: 500px !important;
    }

    .cropper_preview {
        overflow: hidden !important;
        width: 200px !important;
        height: 200px !important;
        margin: 10px !important;
        border: 2px solid black;
    }

    .displaybox {
        width: 30%;
        background-color: #EFEFEF;
        border: none;
        font-size: 12px;
        padding-left: 5px;
    }
</style>
@endsection

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-start mb-0">Profile</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ Auth::user()->type == 'owner' ? route('admin.dashboard') : route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active">Profile</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
    <div id="user-profile">
        <!-- profile header -->
        <div class="row">
            <div class="col-12">
                <div class="card profile-header mb-2" style="background-color: #505050;">

                    <div class="position-relative" style="height: 150px;">
                        <!-- profile picture -->
                        <div class="profile-img-container d-flex align-items-center">
                            <div class="profile-img userImage">
                                <img src="{{ empty($user->image) ? asset('uploads/user.png') : asset('uploads/users/' . $user->image) }}" class="rounded img-fluid" alt="Card image" />
                            </div>
                            <!-- profile title -->
                            <div class="profile-title ms-3">
                                <h2 class="text-white">{{ $user->name }}</h2>
                                <p class="text-white">user</p>
                            </div>
                        </div>
                    </div>

                    <!-- tabs pill -->
                    <div class="profile-header-nav">
                        <!-- navbar -->
                        <nav class="navbar navbar-expand-md navbar-light justify-content-end justify-content-md-between w-100">
                            <button class="btn btn-icon navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <i class="fa-solid fa-align-justify"></i>
                            </button>

                            <!-- collapse  -->
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <div class="profile-tabs d-flex justify-content-between flex-wrap mt-1 mt-md-0">
                                    <ul class="nav nav-pills mb-0">
                                        <li class="nav-item">
                                            <a class="nav-link fw-bold active maintabs" title="About" id="abouttab">
                                                <span class="d-none d-md-block">About</span>
                                                <i class="fa-solid fa-circle-info d-block d-md-none"></i>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link fw-bold maintabs" title="Change About" id="detailstab">
                                                <span class="d-none d-md-block">Change About</span>
                                                <i class="fa-solid fa-user-pen d-block d-md-none"></i>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link fw-bold maintabs" title="Change Password" id="passwordtab">
                                                <span class="d-none d-md-block">Change Password</span>
                                                <i class="fa-solid fa-lock d-block d-md-none"></i>
                                            </a>
                                        </li>
                                        @if ($user['type'] == "owner" && $user['super_type'] == 1)
                                        <li class="nav-item">
                                            <a class="nav-link fw-bold maintabs" title="Company Details" id="companytab">
                                                <span class="d-none d-md-block">Company Details</span>
                                                <i class="fa-solid fa-building d-block d-md-none"></i>
                                            </a>
                                        </li>

                                        @endif
                                        <li class="nav-item">
                                            <form method="POST" action="{{ Auth::user()->type == 'owner' ? route('admin.logout') : route('logout') }}">
                                                @csrf
                                                <button class="nav-link fw-bold maintabs" title="Log Out" type="submit">
                                                    <span class="d-none d-md-block">Log Out</span>
                                                    <i class="fa-solid fa-arrow-right-from-bracket d-block d-md-none"></i>
                                                </button>
                                            </form>
                                        </li>
                                    </ul>
                                    <div class="float-end my-auto">
                                        <h5 class="text-success message d-none">
                                            @if(\Session::has('status'))
                                            {{ \Session::get('status') }}
                                            @endif
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <!--/ collapse  -->
                        </nav>
                        <!--/ navbar -->

                    </div>

                </div>
            </div>
            <!--/ profile header -->
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body tabbody abouttab">
        <h4 class="mb-3"><b>About</b></h4>
        <div class="row">
            <div class="col-md-2"><b>Name</b> :</div>
            <div class="col-md-10 mb-3">{{ $user->name }}</div>
            <div class="col-md-2"><b>Email</b> :</div>
            <div class="col-md-10 mb-3">{{ $user->email }}</div>
            <div class="col-md-2"><b>Number</b> :</div>
            <div class="col-md-10 mb-3">{{ $user->phone }}</div>
            <div class="col-md-2"><b>Joining Date</b> :</div>
            <div class="col-md-10 mb-3">{{ $user->created_at }}</div>
            @if ($user->address)
            <div class="col-md-2"><b>Address</b> :</div>
            <div class="col-md-10 mb-3">{{ $user->address }} - {{ $user->pincode }}</div>
            @endif
        </div>
    </div>

    <div class="card-body tabbody detailstab d-none">
        <h4 class="mb-3"><b>Change About Details</b></h4>
        <form id="changeDetailsForm" class="form" method="POST" action="{{ Auth::user()->type == 'owner' ? route('admin.profile.update') : route('profile.update') }}" enctype="multipart/form-data">
            @csrf
            @method('patch')
            <div class="row">
                <div class="col-md-6 col-12">
                    <div class="mb-1">
                        <label class="form-label" for="name"><b>Name *</b></label>
                        <input type="text" id="name" class="form-control" placeholder="Name" name="name" value="{{ old('name', $user->name) }}" />
                        <p class="text-danger">{{ $errors->first('name') }}</p>
                    </div>
                </div>

                <div class="col-md-6 col-12">
                    <div class="mb-1">
                        <label class="form-label" for="email"><b>Email *</b></label>
                        <input type="text" id="email" class="form-control" placeholder="Email" name="email" value="{{ old('email', $user->email) }}" />
                        <p class="text-danger">{{ $errors->first('email') }}</p>
                    </div>
                </div>

                <div class="col-md-8 col-12">
                    <div class="mb-1">
                        <label class="form-label" for="image">Image</label>
                        <input type="file" id="image" class="form-control" placeholder="Client image or logo" name="image" />
                        <p class="text-danger">{{ $errors->first('image') }}</p>
                    </div>
                </div>
                <div class="col-md-4 col-12" id="imgpr">
                    <div class="mb-1">
                        <img src="{{ empty($user->image) ? asset('uploads/noimg.jpg') : asset('uploads/users/' . $user->image) }}" class="imageclass" id="imgPreview" alt="preview">
                    </div>
                </div>

                <div class="col-12">
                    <button type="submit" class="btn btn-primary me-1">Update</button>
                </div>
            </div>
        </form>
    </div>

    <div class="card-body tabbody passwordtab d-none">
        <h4 class="mb-3"><b>password</b></h4>
        <form id="changePasswordForm" method="post" action="{{ Auth::user()->type == 'owner' ? route('admin.password.update') : route('password.update') }}" class="mt-6 space-y-6">
            @csrf
            @method('put')
            <div class="row">
                <div class="col-md-6 col-12 offset-md-3">
                    <div class="mb-1">
                        <label class="form-label" for="current_password"><b>Current Password *</b></label>
                        <input type="password" id="current_password" class="form-control" placeholder="Current Password" name="current_password" autocomplete="off" />
                        <p class="text-danger">{{ $errors->updatePassword->first('current_password') }}</p>
                    </div>
                </div>

                <div class="col-md-6 col-12 offset-md-3">
                    <div class="mb-1">
                        <label class="form-label" for="password"><b>Password *</b></label>
                        <input type="password" id="password" class="form-control" placeholder="Password" name="password" autocomplete="off" />
                        <p class="text-danger">{{ $errors->updatePassword->first('password') }}</p>
                    </div>
                </div>

                <div class="col-md-6 col-12 offset-md-3">
                    <div class="mb-1">
                        <label class="form-label" for="password_confirmation"><b>Confirm Password *</b></label>
                        <input type="password" id="password_confirmation" class="form-control" placeholder="Confirm Password" name="password_confirmation" autocomplete="off" />
                        <p class="text-danger">{{ $errors->updatePassword->first('password_confirmation') }}</p>
                    </div>
                </div>

                <div class="col-12">
                    <button type="submit" class="btn btn-primary me-1">Change</button>
                </div>
            </div>
        </form>
    </div>

    <div class="card-body tabbody companytab d-none">
        <h4 class="mb-3"><b>Company Details</b></h4>

        <form id="CompanyDetailsForm" class="form" method="POST" action="{{ Auth::user()->type == 'owner' ? route('admin.profile.update-company') : route('profile.update-company') }}" enctype="multipart/form-data">
            @csrf
            @method('patch')
            <div class="row">
                <div class="col-md-6 col-12">
                    <div class="mb-1">
                        <label class="form-label" for="name"><b>Name *</b></label>
                        <input type="text" id="name" class="form-control" placeholder="Name" name="name" value="{{ old('name', $company['name']) }}" />
                        <p class="text-danger">{{ $errors->first('name') }}</p>
                    </div>
                </div>

                <div class="col-md-6 col-12">
                    <div class="mb-1">
                        <label class="form-label" for="phone"><b>Contact Number *</b></label>
                        <input type="text" id="phone" class="form-control" placeholder="9876549875" name="phone" value="{{ old('phone', $company['number']) }}" />
                        <p class="text-danger">{{ $errors->first('phone') }}</p>
                    </div>
                </div>

                <div class="col-md-6 col-12">
                    <div class="mb-1">
                        <label class="form-label" for="email">Email</label>
                        <input type="text" id="email" class="form-control" placeholder="johndoe@example.xyz" name="email" value="{{ old('email', $company['email']) }}" />
                        <p class="text-danger">{{ $errors->first('email') }}</p>
                    </div>
                </div>

                <div class="col-md-6 col-12">
                    <div class="mb-1">
                        <label class="form-label" for="address">Address</label>
                        <textarea name="address" id="address" class="form-control" placeholder="Iscon cross road">{{ old('address', $company['address']) }}</textarea>
                        <p class="text-danger">{{ $errors->first('address') }}</p>
                    </div>
                </div>

                <div class="col-md-3 col-12">
                    <div class="mb-1">
                        <label class="form-label" for="pincode">Pincode</label>
                        <input type="text" id="pincode" class="form-control" placeholder="360001" name="pincode" value="{{ old('pincode', $company['pincode']) }}" />
                        <p class="text-danger">{{ $errors->first('pincode') }}</p>
                    </div>
                </div>
                <div class="col-md-3 col-12">
                    <div class="mb-1">
                        <label class="form-label" for="city">City</label>
                        <select name="city" id="city" class="form-control">
                        </select>
                        <p class="text-danger">{{ $errors->first('city') }}</p>
                    </div>
                </div>
                <div class="col-md-3 col-12">
                    <div class="mb-1">
                        <label class="form-label" for="district">District</label>
                        <input type="text" id="district" class="form-control" placeholder="Ahmedabad" name="district" value="{{ old('district', $company['district']) }}" readonly />
                        <p class="text-danger">{{ $errors->first('district') }}</p>
                    </div>
                </div>
                <div class="col-md-3 col-12">
                    <div class="mb-1">
                        <label class="form-label" for="state">State</label>
                        <div class="input-group">
                            <input type="text" id="state" class="form-control" placeholder="Gujarat" name="state" value="{{ old('state', $company['state']) }}" readonly />
                            <input type="text" id="state_code" class="displaybox" placeholder="State Code" name="state_code" value="{{ old('state_code', $company['state_code']) }}" readonly />
                        </div>
                        <p class="text-danger">{{ $errors->first('state') }}</p>
                    </div>
                </div>

                <div class="col-md-6 col-12">
                    <div class="mb-1">
                        <label class="form-label" for="gst_no">GST No.</label>
                        <input type="text" id="gst_no" class="form-control" placeholder="GST No." name="gst_no" value="{{ old('gst_no', $company['gst_no']) }}" />
                        <p class="text-danger">{{ $errors->first('gst_no') }}</p>
                    </div>
                </div>

                <div class="col-md-6 col-12">
                    <div class="mb-1">
                        <label class="form-label" for="pan_no">Pan No.</label>
                        <input type="text" id="pan_no" class="form-control" placeholder="Pan No." name="pan_no" value="{{ old('pan_no', $company['pan_no']) }}" />
                        <p class="text-danger">{{ $errors->first('pan_no') }}</p>
                    </div>
                </div>

                <div class="col-12">
                    <button type="submit" class="btn btn-primary me-1">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal fade text-start" id="cropImageModal" tabindex="-1" aria-labelledby="modalLabel11" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">Crop Image</h5>
                <button type="button" class="btn-close sendOriginal" data-topre="" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="img-container">
                    <div class="row">
                        <div class="col-md-8">
                            <img src="" id="cropping_image" />
                        </div>
                        <div class="col-md-4">
                            <div class="cropper_preview"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary sendOriginal" data-topre="" data-bs-dismiss="modal" aria-label="Close">Cancel</button>
                <button type="button" class="btn btn-primary" id="crop" data-bs-dismiss="modal" aria-label="Close">Crop</button>
            </div>
        </div>
    </div>
</div>
<button type="button" class="d-none" id="cropImageModalBtn" data-bs-toggle="modal" data-bs-target="#cropImageModal"></button>

@endsection

@section('script')
<script src="{{ asset('app-assets/js/scripts/pages/page-profile.min.js')}}"></script>
<script>
    $(".maintabs").on('click', function() {
        $(".maintabs").removeClass('active');
        $(this).addClass('active');
        let selected = $(this).attr('id');
        $('.tabbody').addClass('d-none');
        $('.' + selected).removeClass('d-none');
    });

    var regionObj = [];
    $('#pincode').on('keyup', function() {
        if ($(this).val().length == 6) {
            var pin = $(this).val();
            fetch("https://api.postalpincode.in/pincode/" + pin)
                .then((res) => {
                    return res.json();
                })
                .then((res) => {
                    let dropdownmenu = '';
                    // $('#district').val(res[0]['PostOffice'][0]['District']);
                    // $("#state").val(res[0]['PostOffice'][0]['State']);
                    regionObj['type-'] = res[0]['PostOffice'];
                    for (let i = 0; i < res[0]['PostOffice'].length; i++) {
                        dropdownmenu += `<option value="${res[0]['PostOffice'][i]['Name']}" data-optionnum="${i}">${res[0]['PostOffice'][i]['Name']}</option>`;
                    }

                    $("#district").val(null);
                    $("#state").val(null);
                    $("#state_code").val(null);
                    $("#city").html(dropdownmenu);
                    $("#city").attr('size', 5);
                })
        } else {
            $('#city').html(null);
            $('#city').attr('size', 1);

            $('#district').val(null);
            $('#state').val(null);
            $("#state_code").val(null);
        }
    });

    function getStateCode(steteName, callback) {
        $.ajax({
            type: 'POST',
            url: "{{ Auth::user()->type == 'owner' ? route('admin.profile.get-statecode') : route('profile.get-statecode') }}",
            data: {
                '_token': '{{ csrf_token() }}',
                'steteName': steteName,
            },
            success: function(data) {
                if (data) {
                    callback(data);
                } else {
                    toastr['error'](`State Code not found!`, 'Error!', {
                        showMethod: 'fadeIn',
                        hideMethod: 'fadeOut',
                        positionClass: 'toast-top-center',
                        timeOut: 3000,
                    });
                }
            }
        });
    }

    $("#city").on("click", function() {
        $(this).attr('size', 1);

        let selectIndex = $(this).find(':selected').data('optionnum');
        $("#district").val(regionObj['type-'][selectIndex]['District']);
        $("#state").val(regionObj['type-'][selectIndex]['State']);
        console.log(regionObj['type-'][selectIndex]['State']);
        let statecode = getStateCode(regionObj['type-'][selectIndex]['State'], function(data) {
            $("#state_code").val(data);
        });
    });

    $('#gst_no').on('keyup', function() {
        let gstin = $(this).val();
        if (gstin.length == 15) {
            $("#pan_no").val(gstin.substring(2, gstin.length - 3));
        }
    });

    jQuery.validator.addMethod('fileSizeLimit', function(value, element, limit) {
        return !element.files[0] || (element.files[0].size <= limit);
    }, 'Image is too big');

    var validationForm = $('#changeDetailsForm');
    validationForm.validate({
        rules: {
            'name': {
                required: true,
            },
            'email': {
                required: true,
                email: true,
            },
            'image': {
                required: false,
                fileSizeLimit: 2048000,
                extension: "jpg|png|jpeg|gif|svg"
            }
        },
        messages: {
            'name': {
                required: "Name is require",
            },
            'email': {
                required: "Email is require",
                email: "Invalid email format"
            },
            'image': {
                required: "Image is require",
                extension: "Please select image file"
            }
        }
    });

    var validationForm2 = $('#changePasswordForm');
    validationForm2.validate({
        rules: {
            'current_password': {
                required: true,
            },
            'password': {
                required: true,
            },
            'password_confirmation': {
                required: true,
                equalTo: '#password'
            }
        },
        messages: {
            'current_password': {
                required: "Current password is require",
            },
            'password': {
                required: "Password is require",
            },
            'password_confirmation': {
                required: "Confirm password is require",
                equalTo: "Passwords does not match"
            }
        }
    });

    var validationForm3 = $('#CompanyDetailsForm');
    validationForm3.validate({
        rules: {
            'name': {
                required: true,
            },
            'phone': {
                required: true,
                digits: true,
                maxlength: 10,
                minlength: 10
            },
            'gst_no': {
                required: false,
                gstCheck: true,
            },
            'pan_no': {
                required: false,
                panCheck: true,
            },
            'email': {
                required: false,
                email: true,
            },
            'address': {
                required: true,
            },
            'city': {
                required: true,
            },
            'district': {
                required: true,
            },
            'state': {
                required: true,
            },
            'pincode': {
                required: true,
                digits: true,
                maxlength: 6,
                minlength: 6
            },
        },
        messages: {
            'name': {
                required: "Name is require",
            },
            'phone': {
                required: "Number is require",
                digits: "Number must be in digits",
                maxlength: "Length must be 10 digits",
                minlength: "Length must be 10 digits"
            },
            'email': {
                required: "Email is require",
                email: "Invalid email format",
            },
            'address': {
                required: "Address is require",
            },
            'city': {
                required: "City is require",
            },
            'district': {
                required: "District is require",
            },
            'state': {
                required: "State is require",
            },
            'pincode': {
                required: "Pincode is require",
                digits: "Pincode must be in digits",
                maxlength: "Length must be 6 digits",
                minlength: "Length must be 6 digits"
            },
        }
    });

    var validationForm4 = $('#invoiceSettingForm');
    validationForm4.validate({
        rules: {
            'discount_type': {
                required: true,
            },
        },
        messages: {
            'discount_type': {
                required: "Discount Type is require",
            },
        }
    });

    $(document).ready(() => {
        $("#image").change(function() {
            const file = this.files[0];
            if (file) {
                let reader = new FileReader();
                reader.onload = function(event) {
                    $("#imgPreview").attr("src", event.target.result);
                };
                reader.readAsDataURL(file);
            }
        });


        var $modal = $('#cropImageModal');
        var image = document.getElementById('cropping_image');
        var cropper;

        $('#image').change(function(event) {
            var files = event.target.files;
            var done = function(url) {
                image.src = url;
                $modal.modal('show');
            };

            if (files && files.length > 0) {
                reader = new FileReader();
                reader.onload = function(event) {
                    done(reader.result);
                };
                reader.readAsDataURL(files[0]);
            }
        });

        $modal.on('shown.bs.modal', function() {
            cropper = new Cropper(image, {
                multiple: true,
                aspectRatio: 1,
                preview: '.cropper_preview'
            });
        }).on('hidden.bs.modal', function() {
            cropper.destroy();
            cropper = null;

        });

        $("#crop").click(function() {
            canvas = cropper.getCroppedCanvas({
                width: 500,
                height: 500,
            });

            canvas.toBlob(function(blob) {
                var reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onloadend = function() {
                    var base64data = reader.result;
                    $("#imgPreview").attr("src", base64data);

                    function dataURLtoFile(dataurl, filename) {
                        var arr = dataurl.split(','),
                            mime = arr[0].match(/:(.*?);/)[1],
                            bstr = atob(arr[arr.length - 1]),
                            n = bstr.length,
                            u8arr = new Uint8Array(n);
                        while (n--) {
                            u8arr[n] = bstr.charCodeAt(n);
                        }
                        return new File([u8arr], filename, {
                            type: mime
                        });
                    }

                    var file = dataURLtoFile(base64data, 'image.png');
                    const fileInput = document.getElementById('image');

                    const dataTransfer = new DataTransfer();
                    dataTransfer.items.add(file);
                    fileInput.files = dataTransfer.files;
                }
            });
        });
    });
</script>
@if($errors->updatePassword->any())
<script>
    $("#passwordtab").click();
</script>
@endif
@if($errors->any())
<script>
    $("#detailstab").click();
</script>
@endif
@if(\Session::has('status'))
<script>
    $(".message").removeClass('d-none');
    setTimeout(() => {
        $(".message").addClass('d-none');
    }, 3000);
</script>
@endif

@if ($company['city'])
<script>
    let pin = "{!! $company['pincode'] !!}";
    let city = "{!! $company['city'] !!}";
    fetch("https://api.postalpincode.in/pincode/" + pin)
        .then((res) => {
            return res.json();
        })
        .then((res) => {
            let dropdownmenu = '';
            for (let i = 0; i < res[0]['PostOffice'].length; i++) {
                if (city == res[0]['PostOffice'][i]['Name']) {
                    dropdownmenu += `<option value="${res[0]['PostOffice'][i]['Name']}" selected>${res[0]['PostOffice'][i]['Name']}</option>`;
                } else {
                    dropdownmenu += `<option value="${res[0]['PostOffice'][i]['Name']}">${res[0]['PostOffice'][i]['Name']}</option>`;
                }
            }

            $("#city").html(dropdownmenu);
        })
</script>
@endif
@endsection
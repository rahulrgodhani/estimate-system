<link rel="apple-touch-icon" href="{{ asset('uploads/config/ico/favicon.png') }}">
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('uploads/config/ico/favicon.png') }}">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />

<!-- BEGIN: Vendor CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/vendors.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
<!-- END: Vendor CSS-->

<!-- BEGIN: Theme CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap-extended.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/colors.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/components.min.css') }}">
<!-- END: Theme CSS-->

<!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/form-validation.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
<!-- END: Page CSS-->

<!-- BEGIN: Custom CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
<script src="https://js.pusher.com/8.0.1/pusher.min.js"></script>
<style>
    .loader {
        background-image: url("{{ asset('uploads/config/logo/logo.png') }}");
    }

    .sidebarsearchoptions {
        position: absolute;
        margin: 5px 15px 0 15px !important;
        z-index: 2;
        background: whitesmoke;
        border-radius: 5px;
    }

    .liitems {
        border-bottom: 1px solid gray;
        padding: 0 15px;
        width: 100%;
    }

    .readnotifications {
        opacity: 0.5;
    }
</style>
<!-- END: Custom CSS-->
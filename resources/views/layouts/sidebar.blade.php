<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item me-auto mainlogoarea">
                <a class="navbar-brand"
                    href="{{ Auth::user()->type == 'owner' ? route('admin.dashboard') : route('dashboard') }}">
                    <span class="brand-logo">
                        <img class="mainlogoimage"
                            src="{{ asset('uploads/config/logo/logo-icon.png') }}?rand=<?php echo rand(); ?>"
                            alt="Logo">
                    </span>
                    <h2 class="brand-text mainlogotext">{{ App\Models\Setting::find(1)->name }}</h2>
                </a>
            </li>
            <li class="nav-item nav-toggle mainlogocollpasearea"><a class="nav-link modern-nav-toggle pe-0"
                    data-bs-toggle="collapse"><i
                        class="d-block d-xl-none text-primary toggle-icon font-medium-4 fa-solid fa-x"></i><i
                        class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary  fa-solid fa-compact-disc"></i></a>
            </li>
        </ul>
    </div>

    <div class="shadow-bottom"></div>

    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

            <li class="nav-item" style="margin-top: calc(2rem + 0.286rem);">
                <a class="d-flex align-items-center" style=" background-color: #FFF !important; box-shadow: none;">
                    <i class="fa-solid fa-magnifying-glass" style="color: #615f6e;"></i>
                    <span class="menu-title text-truncate"><input type="text" class="form-control" id="searchsidebar"
                            placeholder="Search"></span>
                </a>
            </li>
            <ul class="sidebarsearchoptions" id="searchresult"></ul>
            @php($i = 0)

            @foreach (App\Models\SidemenuAccess::join('sidemenu', 'sidemenu_access.page_id', '=', 'sidemenu.id')->where('sidemenu_access.ws_status', Auth::user()->type)->where('sidemenu.parent_id', 0)->where('sidemenu.status', 1)->where('sidemenu_access.admin_status', 1)->where('sidemenu_access.user_id', Auth::user()->id)->orderBy('sidemenu.ordering', 'ASC')->get() as $singleItem)
                @if ($singleItem['route_name'] == '#')
                    <li class="nav-item has-sub">
                        <a class="d-flex align-items-center" href="#">
                            <i class="fa-solid fa-{{ $singleItem['icon'] }}"></i>
                            <span class="mmun menu-title text-truncate"
                                data-i18n="{{ $singleItem['icon'] }}">{{ $singleItem['page_name'] }}</span>
                        </a>
                        <ul class="menu-content">

                            @foreach (App\Models\SidemenuAccess::join('sidemenu', 'sidemenu_access.page_id', '=', 'sidemenu.id')->where('sidemenu_access.ws_status', Auth::user()->type)->where('sidemenu.parent_id', $singleItem['id'])->where('sidemenu.status', 1)->where('sidemenu_access.admin_status', 1)->where('sidemenu_access.user_id', Auth::user()->id)->orderBy('sidemenu.ordering', 'ASC')->get() as $dowpdownItem)
                                @php($dropref = (Auth::user()->type == 'owner' ? 'admin.' : '') . $dowpdownItem['route_reference'])
                                @php($droproute = (Auth::user()->type == 'owner' ? 'admin.' : '') . $dowpdownItem['route_name'])
                                <li
                                    class="nav-item {{ strpos(Route::currentRouteName(), $dropref) === 0 ? 'active' : '' }}">
                                    <a class="d-flex align-items-center" href="{{ route($droproute) }}">
                                        <i class="fa-solid fa-{{ $dowpdownItem['icon'] }}"></i>
                                        <span class="mmun menu-item text-truncate"
                                            data-i18n="{{ $dowpdownItem['icon'] }}">{{ $dowpdownItem['page_name'] }}</span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @elseif($singleItem['route_name'] == 'span')
                    <li class="navigation-header">
                        <span data-i18n="Data Collection">{{ $singleItem['page_name'] }}</span>
                    </li>
                @else
                    @php($ref = (Auth::user()->type == 'owner' ? 'admin.' : '') . $singleItem['route_reference'])
                    @php($route = (Auth::user()->type == 'owner' ? 'admin.' : '') . $singleItem['route_name'])
                    <li class="nav-item {{ strpos(Route::currentRouteName(), $ref) === 0 ? 'active' : '' }}"
                        @if ($i == 0) style="margin-top: calc(2rem + 0.286rem)" @endif>
                        <a class="d-flex align-items-center" href="{{ route($route) }}">
                            <i class="fa-solid fa-{{ $singleItem['icon'] }}"></i>
                            <span class="mmun menu-title text-truncate"
                                data-i18n="{{ $singleItem['icon'] }}">{{ $singleItem['page_name'] }}</span>
                        </a>
                    </li>
                @endif
                @php($i++)
            @endforeach

        </ul>
    </div>
</div>

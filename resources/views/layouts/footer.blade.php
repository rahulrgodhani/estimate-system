<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<footer class="footer footer-static footer-light">
    <p class="clearfix mb-0">
        <span class="float-md-end d-none d-md-block">Hand-crafted & Made with<i class="fa-regular fa-heart"></i></span>
    </p>
</footer>
<button class="btn btn-primary btn-icon scroll-top" type="button"><i class="fa-solid fa-arrow-up"></i></button>